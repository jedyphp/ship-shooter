using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponRotation : MonoBehaviour
{
    private Canvas _canvas;
    private Vector3 _direction;
    void Start()
    {

    }

    private void FixedUpdate()
    {
        Vector3 mousePos = Input.mousePosition;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x -= objectPos.x;
        mousePos.y -= objectPos.y;

        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90f));

        _direction = new Vector3(mousePos.x, mousePos.y, 0).normalized;
    }

    public void OnWeaponClick()
    {
        GetComponent<Rigidbody2D>().AddForce(_direction * 100f, ForceMode2D.Force);
    }
}
