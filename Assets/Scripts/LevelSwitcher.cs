using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LevelSwitcher : MonoBehaviour
{
    private void Start()
    {
        Button[] buttons = GetComponentsInChildren<Button>();
        foreach (Button button in buttons)
        {
            button.onClick.AddListener(() => { Switch(button); });
        }
    }

    private void Switch(Button button)
    {
        string text = button.GetComponentInChildren<TextMeshProUGUI>().text;

        if (text != null)
        {
            SceneManager.LoadScene(text.Trim());
        }
        else
        {
            Debug.LogWarning("Empty button title");
        }
    }
}
