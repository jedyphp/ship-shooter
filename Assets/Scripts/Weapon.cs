using UnityEngine;

public class Weapon : MonoBehaviour
{ 
    private bool _isMoving;

    public bool IsMoving { get => _isMoving; set => _isMoving = value; }
}
