using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.TryGetComponent(out Weapon weapon))
        {
            Destroy(weapon.gameObject);
        }

        if (collision.collider.TryGetComponent(out Enemy enemy))
        {
            // Destroy(enemy.gameObject);
        }
    }
}
