using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    private Vector2 _direction;

    private Vector2 _startPoint;
    private RectTransform _canvas;
    private Image _image;
    private Weapon _weapon;
    private const float START_HEIGHT = 250f;
    public delegate void EnemyHitAction();

    private List<Vector2> _directions = new List<Vector2> { Vector2.left, Vector2.right };

    public Vector2 Direction { get => _direction; set => _direction = value; }
    public Vector2 StartPoint { get => _startPoint; set => _startPoint = value; }

    public static event EnemyHitAction OnEnemyHit;

    [SerializeField] private ParticleSystem _particleSystem;

    private void Awake()
    {
        GetDirections();
    }

    private void Start()
    {
        WeaponShooter.OnEndGame += StopMovement;
        EnemyGenerator.OnEndGame += StopMovement;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.TryGetComponent(out Weapon weapon))
        {
            OnEnemyHit?.Invoke();
            _weapon = weapon;
            _particleSystem.Play();
            Destroy(_weapon.gameObject);
            StartCoroutine(DelayedDestroy(1f));
        }

        // If the collision with another enemy happened
        if (collision.collider.TryGetComponent(out Enemy otherEnemy))
        {
            Destroy(gameObject);
            Destroy(otherEnemy.gameObject);
        }
    }

    private void StopMovement()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().angularVelocity = 0f;
    }

    private void OnDisable()
    {
        WeaponShooter.OnEndGame -= StopMovement;
        EnemyGenerator.OnEndGame -= StopMovement;
    }

    private void GetDirections()
    {
        _canvas = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        _image = GetComponent<Image>();

        switch (SceneManager.GetActiveScene().name)
        {
            case Levels.Level1:
                StartPoint = new Vector2(0, Random.Range(START_HEIGHT, _canvas.rect.height));
                Direction = Vector2.right;
                break;
            case Levels.Level2:
                StartPoint = new Vector2(_canvas.rect.width, Random.Range(START_HEIGHT, _canvas.rect.height));
                Direction = Vector2.left;
                _image.gameObject.transform.localScale = new Vector3(-1, 1, 1);
                GetComponentInChildren<ParticleSystem>().transform.localScale = new Vector3(-1, 1, 1);
                break;
            default:
                Direction = _directions[Random.Range(0, _directions.Count)];
                float point = Direction == Vector2.right ? 0 : _canvas.rect.width;
                StartPoint = new Vector2(point, Random.Range(START_HEIGHT, _canvas.rect.height));
                // Flip image if this is opposite side
                if (Direction == Vector2.left)
                {
                    _image.gameObject.transform.localScale = new Vector3(-1, 1, 1);
                    GetComponentInChildren<ParticleSystem>().transform.localScale = new Vector3(-1, 1, 1);
                }
                break;
        }
    }

    private IEnumerator DelayedDestroy(float delay)
    {
        yield return new WaitForSeconds(delay);

        Destroy(gameObject);
    }
}
