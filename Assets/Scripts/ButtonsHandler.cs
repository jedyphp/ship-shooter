using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ButtonsHandler : MonoBehaviour
{
    public void OnMainMenuClick()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnRestartClick()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
