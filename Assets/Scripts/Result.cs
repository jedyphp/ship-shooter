using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Result : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }
    private void Start()
    {
        text.SetText(PlayerPrefs.GetInt("EnemiesHit").ToString());
        Enemy.OnEnemyHit += UpdateCounter;
    }

    private void UpdateCounter()
    {
        text.SetText(PlayerPrefs.GetInt("EnemiesHit").ToString());
    }

    private void OnDisable()
    {
        Enemy.OnEnemyHit -= UpdateCounter;
    }
}
