using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class WeaponShooter : MonoBehaviour, IPointerClickHandler
{
    private Vector3 _direction;
    private int _weaponLeft;
    private Weapon _pendingWeapon;
    private float _multiplier;
    private const float BASE_SPEED = 6.0f;

    [SerializeField] private Weapon _weaponPrefab;
    [SerializeField] private Transform _container;
    [SerializeField] private MessagePopup _messagePopup;

    public delegate void EndGameAction();

    public static event EndGameAction OnEndGame;

    public delegate void WeaponCountAction(int count);

    public static event WeaponCountAction OnWeaponCount;


    private const int COUNT_WEAPON = 10;
    private bool _isLoading; // prevent too fast clicks

    private void Start()
    {
        _weaponLeft = COUNT_WEAPON;
        _multiplier = GetMultiplier();
        OnWeaponCount?.Invoke(_weaponLeft);

        GenerateNew();
    }

    private void FixedUpdate()
    {
        // block looking at cursor while moving
        if (_pendingWeapon != null && !_pendingWeapon.IsMoving)
        {
            Vector3 mousePos = Input.mousePosition;
            Vector3 objectPos = Camera.main.WorldToScreenPoint(_pendingWeapon.gameObject.transform.position);
            mousePos.x -= objectPos.x;
            mousePos.y -= objectPos.y;

            float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            _pendingWeapon.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90f));

            _direction = new Vector3(mousePos.x, mousePos.y, 0).normalized;
        }

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!_isLoading)
        {
            _isLoading = true;
            _pendingWeapon.IsMoving = true;
            _pendingWeapon.GetComponent<Rigidbody2D>().AddForce(_direction * BASE_SPEED * _multiplier, ForceMode2D.Impulse);
            _weaponLeft--;
            OnWeaponCount?.Invoke(_weaponLeft);

            if (_weaponLeft > 0)
            {
                StartCoroutine(DelayedGenerate(0.3f));
            }
            else
            {
                _messagePopup.ShowDefeat();
                OnEndGame?.Invoke();
            }
        }
    }

    private void GenerateNew()
    {
        _pendingWeapon = Instantiate(_weaponPrefab, _container, false) as Weapon;
        _isLoading = false;
    }

    private IEnumerator DelayedGenerate(float delay)
    {
        yield return new WaitForSeconds(delay);
        GenerateNew();
    }

    private float GetMultiplier()
    {
        if (SceneManager.GetActiveScene().name == Levels.Level3)
        {
            return 2f;
        }

        return 1f;
    }
}
