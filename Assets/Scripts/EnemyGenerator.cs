using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyGenerator : MonoBehaviour
{
    private const int WIN_COUNT = 3;
    private int _enemiesHit;
    private Coroutine _generator;
    private float _screenHeight;
    private float _screenWidth;
    private List<Vector2> _directions = new List<Vector2>();
    private Vector2 _position;
    private const float BASE_SPEED = 1500f;

    [SerializeField] private Enemy _enemyPrefab;
    [SerializeField] private Transform _container;
    [SerializeField] private MessagePopup _messagePopup;

    public delegate void EndGameAction();

    public static event EndGameAction OnEndGame;

    private void Awake()
    {
        // Get directions based on current scene
        // The best of all to move these all to config
        // But for prototyping this should be enough
        // We don't have a lot of config to create interface to work with them

        if (!PlayerPrefs.HasKey("EnemiesHit"))
        {
            PlayerPrefs.SetInt("EnemiesHit", 0);
        }

        PlayerPrefs.SetInt("CurrentSession", 0);
        Enemy.OnEnemyHit += UpdateCounter;
    }

    private void Start()
    {
        _enemiesHit = 0;
        _generator = StartCoroutine(GenerateEnemy(2f));

        string scene = SceneManager.GetActiveScene().name;
        WeaponShooter.OnEndGame += StopGenerating;
    }


    private IEnumerator GenerateEnemy(float delay)
    {
        do
        {
            Transform transform = GetComponent<Transform>();

            Enemy enemy = Instantiate(_enemyPrefab, _container, false);
            enemy.gameObject.GetComponent<RectTransform>().anchoredPosition = enemy.StartPoint;

            Rigidbody2D rigidBody = enemy.GetComponent<Rigidbody2D>();
            if (rigidBody != null) rigidBody.AddForce(enemy.Direction * BASE_SPEED, ForceMode2D.Force);

            yield return new WaitForSeconds(delay);

        } while (true);
    }

    private void StopGenerating()
    {
        StopCoroutine(_generator);
    }

    private void UpdateCounter()
    {
        _enemiesHit++;
        UpdatePrefs();

        if (_enemiesHit == WIN_COUNT)
        {
            StopCoroutine(_generator);
            _messagePopup.ShowSuccess();
            OnEndGame?.Invoke();
        } // show message
    }

    private void OnDisable()
    {
        Enemy.OnEnemyHit -= UpdateCounter;
        WeaponShooter.OnEndGame -= StopGenerating;
    }

    private void UpdatePrefs()
    {
        // update common progress
        int counter = PlayerPrefs.GetInt("EnemiesHit");
        PlayerPrefs.SetInt("EnemiesHit", ++counter);

        // update session progress
        PlayerPrefs.SetInt("CurrentSession", _enemiesHit);
    }
}
