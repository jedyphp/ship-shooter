using UnityEngine;
using TMPro;

public class WeaponLeft : MonoBehaviour
{
    private void Awake()
    {
        // Enemy.OnEnemyHit += UpdateCounter;
        WeaponShooter.OnWeaponCount += UpdateCounter;
    }

    private void UpdateCounter(int count)
    {
        GetComponent<TextMeshProUGUI>().SetText(count.ToString());
    }

    private void OnDisable()
    {
        WeaponShooter.OnWeaponCount -= UpdateCounter;
    }
}
