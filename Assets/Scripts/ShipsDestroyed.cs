using UnityEngine;
using TMPro;

public class ShipsDestroyed : MonoBehaviour
{
    private void OnEnable()
    {
        TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
        text.SetText(PlayerPrefs.GetInt("CurrentSession").ToString());
    }

}
