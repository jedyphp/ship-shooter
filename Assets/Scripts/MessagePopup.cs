using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagePopup : MonoBehaviour
{
    [SerializeField] private Transform _successPopup;
    [SerializeField] private Transform _defeatPopup;
    void Start()
    {
        
    }

    public void ShowSuccess() { 
        gameObject.SetActive(true);
        _successPopup.gameObject.SetActive(true);
    }

    public void ShowDefeat() { 
        gameObject.SetActive(true);
        _defeatPopup.gameObject.SetActive(true);
    }
}
